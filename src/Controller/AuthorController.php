<?php

namespace App\Controller;

use App\Service\AuthorService;
use App\Service\TokenProvider;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class AuthorController extends ApplicationController
{
    public function __construct(
        private readonly AuthorService $authorService,
        private readonly TokenProvider $tokenProvider
    ) {
        parent::__construct($this->tokenProvider);
    }

    #[Route('/authors', name: 'listing_authors')]
    public function index(): Response
    {
        if (!$this->isLoggedIn()) {
            return $this->redirectToRoute('app_login_get');
        }

        $authors = $this->authorService->getAuthors();

        return $this->render('author/index.html.twig', compact('authors'));
    }

    #[Route('/authors/{id}', name: 'author_single')]
    public function single(Request $request): Response
    {
        if (!$this->isLoggedIn()) {
            return $this->redirectToRoute('app_login_get');
        }

        $client = HttpClient::create();

        $data = $client->request('GET', 'https://symfony-skeleton.q-tests.com/api/v2/authors/'.$request->get('id'), [
            'headers' => [
                'Authorization' => 'Bearer '.$this->tokenProvider->getToken(),
            ],
        ]
        );

        $author = $data->toArray();

        return $this->render('author/single.html.twig', compact('author'));
    }

    #[Route('/authors/{id}/delete', name: 'author_delete')]
    public function delete(Request $request): Response
    {
        if (!$this->isLoggedIn()) {
            return $this->redirectToRoute('app_login_get');
        }

        $client = HttpClient::create();

        $data = $client->request('GET', 'https://symfony-skeleton.q-tests.com/api/v2/authors/'.$request->get('id'), [
            'headers' => [
                'Authorization' => 'Bearer '.$this->tokenProvider->getToken(),
            ],
        ]
        );

        $author = $data->toArray();

        if (count($author['books'])) {
            $this->addFlash('warning', 'Cannot delete... has books');

            return $this->redirectToRoute('listing_authors');
        }

        $client->request('DELETE', 'https://symfony-skeleton.q-tests.com/api/v2/authors/'.$request->get('id'), [
            'headers' => [
                'Authorization' => 'Bearer '.$this->tokenProvider->getToken(),
            ],
        ]
        );

        $this->addFlash('warning', 'Deleted');

        return $this->redirectToRoute('listing_authors');
    }
}
