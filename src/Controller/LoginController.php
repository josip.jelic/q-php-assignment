<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class LoginController extends AbstractController
{
    #[Route('/login', name: 'app_login_get', methods: ['GET'])]
    public function index(Request $request): Response
    {
        return $this->render('login/index.html.twig');
    }

    #[Route('/login', name: 'app_login_post', methods: ['POST'])]
    public function login(Request $request): Response
    {
        $client = HttpClient::create();

        $data = $client->request('POST', 'https://symfony-skeleton.q-tests.com/api/v2/token', [
            'json' => [
                'email' => $request->get('email'),
                'password' => $request->get('password'),
            ],
        ]
        );

        $status = $data->getStatusCode();

        if (200 !== $status) {
            $this->addFlash('error', 'Wrong email or password');

            return $this->redirectToRoute('app_login_get');
        }

        $session = $request->getSession();

        $session->set('token', $data->toArray()['token_key']);
        $session->set('user', $data->toArray()['user']);

        return $this->redirectToRoute('app_home');
    }

    #[Route('/logout', name: 'app_logout', methods: ['GET'])]
    public function logout(Request $request): Response
    {
        $session = $request->getSession();

        $session->set('token', null);

        return $this->redirectToRoute('app_home');
    }
}
