<?php

namespace App\Controller;

use App\Service\AuthorService;
use App\Service\TokenProvider;
use Carbon\Carbon;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class BookController extends ApplicationController
{
    public function __construct(
        private readonly AuthorService $authorService,
        private readonly TokenProvider $tokenProvider
    ) {
        parent::__construct($this->tokenProvider);
    }

    #[Route('/book', name: 'add_book', methods: 'GET')]
    public function index(): Response
    {
        if (!$this->isLoggedIn()) {
            return $this->redirectToRoute('app_login_get');
        }

        $authors = $this->authorService->getAuthors();

        return $this->render('book/add.html.twig', compact('authors'));
    }

    #[Route('/book', name: 'add_book_post', methods: 'POST')]
    public function add_book(Request $request): Response
    {
        if (!$this->isLoggedIn()) {
            return $this->redirectToRoute('app_login_get');
        }

        $authors = $this->authorService->getAuthors();

        $client = HttpClient::create();

        $data = $client->request('POST', 'https://symfony-skeleton.q-tests.com/api/v2/books', [
            'json' => [
                'author' => ['id' => $request->get('author_id')],
                'title' => $request->get('title'),
                'release_date' => Carbon::parse($request->get('release_date')),
                'isbn' => $request->get('isbn'),
                'format' => $request->get('format'),
                'description' => $request->get('description'),
                'number_of_pages' => (int) $request->get('number_of_pages'),
            ],
            'headers' => [
                'Authorization' => 'Bearer '.$this->tokenProvider->getToken(),
            ],
        ]
        );

        if (200 !== $data->getStatusCode()) {
            $this->addFlash('warning', 'Validation failed');

            return $this->render('book/add.html.twig', compact('authors'));
        }

        $this->addFlash('warning', 'Book added');

        return $this->render('book/add.html.twig', compact('authors'));
    }

    #[Route('/authors/{author_id}/books/{book_id}/delete', name: 'book_delete')]
    public function delete(Request $request): Response
    {
        if (!$this->isLoggedIn()) {
            return $this->redirectToRoute('app_login_get');
        }
        $client = HttpClient::create();

        $client->request('DELETE', 'https://symfony-skeleton.q-tests.com/api/v2/books/'.$request->get('book_id'), [
            'headers' => [
                'Authorization' => 'Bearer '.$this->tokenProvider->getToken(),
            ],
        ]
        );

        $this->addFlash('warning', 'Deleted');

        return $this->redirectToRoute('author_single', ['id' => $request->get('author_id')]);
    }
}
