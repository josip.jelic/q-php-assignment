<?php

namespace App\Controller;

use App\Service\TokenProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ApplicationController extends AbstractController
{
    public function __construct(
        private readonly TokenProvider $tokenProvider
    ) {
    }

    public function isLoggedIn(): bool
    {
        if (!$this->tokenProvider->getToken()) {
            return false;
        }

        return true;
    }
}
