<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class IndexController extends AbstractController
{
    #[Route('/', name: 'app_home', methods: ['GET'])]
    public function index(Request $request): Response
    {
        $session = $request->getSession();

        if (!$session->get('token')) {
            return $this->redirectToRoute('app_login_get');
        }

        $user = $session->get('user');

        return $this->render('index/index.html.twig', compact('user'));
    }
}
