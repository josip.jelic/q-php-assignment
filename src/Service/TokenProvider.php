<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\RequestStack;

class TokenProvider
{
    public function __construct(private readonly RequestStack $requestStack)
    {
    }

    public function getToken()
    {
        return $this->requestStack->getSession()->get('token');
    }
}
