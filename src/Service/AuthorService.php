<?php

namespace App\Service;

use Carbon\Carbon;
use Symfony\Component\HttpClient\HttpClient;

class AuthorService
{
    public function __construct(private readonly TokenProvider $tokenProvider)
    {
    }

    public function getAuthors(): array
    {
        $client = HttpClient::create();

        $data = $client->request('GET', 'https://symfony-skeleton.q-tests.com/api/v2/authors', [
            'headers' => [
                'Authorization' => 'Bearer '.$this->tokenProvider->getToken(),
            ],
        ]
        );

        return $data->toArray()['items'];
    }

    public function createAuthor(string $firstName, string $lastName, Carbon $birthday, string $gender, string $placeOfBirth, string $token): bool
    {
        $client = HttpClient::create();

        $data = $client->request('POST', 'https://symfony-skeleton.q-tests.com/api/v2/authors', [
            'headers' => [
                'Authorization' => 'Bearer '.$token,
            ],
            'json' => [
                'first_name' => $firstName,
                'last_name' => $lastName,
                'birthday' => $birthday->toAtomString(),
                'gender' => $gender,
                'place_of_birth' => $placeOfBirth,
            ],
        ]
        );

        return 200 === $data->getStatusCode();
    }
}
