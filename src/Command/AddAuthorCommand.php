<?php

namespace App\Command;

use App\Service\AuthorService;
use Carbon\Carbon;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpClient\HttpClient;

#[AsCommand(
    name: 'author:add',
    description: 'Add a short description for your command',
)]
class AddAuthorCommand extends Command
{
    public function __construct(readonly private AuthorService $authorService)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $firstName = $io->ask('Enter First name');
        $lastName = $io->ask('Enter Last name');
        $birthday = Carbon::parse($io->ask('Enter Birthday'));
        $gender = $io->ask('Enter Gender');
        $placeOfBirth = $io->ask('Enter Place of Birth');

        $success = $this->authorService->createAuthor($firstName, $lastName, $birthday, $gender, $placeOfBirth, $this->getToken());

        if ($success) {
            $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

            return Command::SUCCESS;
        }

        $io->error('Something went wrong');

        return Command::FAILURE;
    }

    private function getToken()
    {
        $client = HttpClient::create();

        $data = $client->request('POST', 'https://symfony-skeleton.q-tests.com/api/v2/token', [
            'json' => [
                'email' => 'ahsoka.tano@q.agency',
                'password' => 'Kryze4President',
            ],
        ]
        );

        return $data->toArray()['token_key'];
    }
}
